# ansible-role-users

Ansible role to configure users (with SSH keys), groups and permissions and
SSH based on roles and subjects (persons) as defined in group_vars and host_vars.

This Ansible role has two modes of operation:

1. Configure a user account per role and add SSH keys for each subject (default)
1. Configure a user account for each subject

## Example playbook

```yaml
- hosts: all
  gather_facts: true
  become: false
  tasks:
    - include_role:
        name: users
```

## Variables

### Users, groups and roles

Configure accounts per role and not for each individual subject (default):

```yaml
user_role_accounts: true
```

Delete users and homedir from machines:

```yaml
user_accounts_absent:
  - fred
```

Also undefined is allowed:

```yaml
# user_accounts_absent:
```

Full example with user related variables:

```yaml
# user accounts that should not exist
user_accounts_absent:
  - fred

# the role(s) to enable
user_roles_enabled:
  - operations
  - development

# the users per team and root permission
user_roles:
  - name: operations
    account: ops
    subjects:
      - caroline
      - jacob
      - rebecca
    permissions:
      sudo: true
      groups:
        - dialout
  - name: development
    account: dev
    subjects:
      - oscar
      - james
    permissions:
      sudo: false
      groups:
        - specialgroup

# User base per user
# shell defaults to bash
# make sure to install other type of shells otherwhise when configured here
# ssh defaults to rsa

user_subjects:
  - name: caroline
    key_type: ssh-ed25519
    public_key: AAAA

  - name: jacob
    public_key: AAAA

  - name: rebecca
    public_key: AAAA

  - name: oscar
    shell: /bin/zsh
    public_key: AAAAB

  - name: james
    public_key: AAAAB
```

### SSH

Set nothing:

```yaml
sshd_listen_address: none
```

Or set 0.0.0.0:

```yaml
sshd_listen_address: any
```

or set `{{ ansible_host }}`:

```yaml
sshd_listen_address: ansible_host  # Ubuntu: sshd won't listen on the Floating IP when this is not a local address.
```

or one IP address:

```yaml
sshd_listen_address: 10.0.0.1
```

NOT an iprange:

```yaml
sshd_listen_address: 10.0.0.1/24 # Works for ansible but _not_ for sshd.
```

Or as list:

```yaml
sshd_listen_address:
  - 10.0.0.1
  - 10.0.0.2
```

Default:

```yaml
sshd_listen_address: none
```

Demo:

```yaml
sshd_listen_address: "{{ ansible_default_ipv4['address'] }}"
```
 or
```yaml
sshd_listen_address: ansible_host 
```